from os import execlp

#This part of the code opens the text file, into which the code is going to write all entered data. 

fhand = open('lotto.txt')  
for line in fhand:                          #Here the loop has been created
    line = line.rstrip()                   # Strip whitespace from right side of string
    if not line.startswith('Ticket '): continue  #Program is picking out lines starting with word Ticket
    words = line.split()                  #splits text in the file into words and calls the new variable words 
    num = (words[1])                    # variable called num is picking out the word of second place in the previous line
    delimiter = ' '
p = delimiter.join(num)             #translate the value from list to string
top = int(p)                        #makes an integer out of the value
ticket = (top+1)                    #adds one to number picked from the line in the file and calls it ticket

name = input('Enter any name of yours: ')  #imput name of player

with open("lotto.txt","a") as f:  #opens the file as variable f
        
    f.write("\nTicket ")           #writes a word Ticket to new line in lotto text file
    f.write(str(ticket))            # cconvert ticket value from integer to string and write it to line
    f.write(" ")                    #writes space
    f.write(name)                   #writes inputed the name of the player to the line
    f.write(" ")                    #writes space

def check_guess(gs,range):          #creates a function called check_guess
        still_guessing = True       #the function is for checking if input guess is number and also in range of 32
        attempt = 0

        while still_guessing and attempt < 10: #creates a loop that repeats at most up to 10 times
            
            if gs.lower() in range :            #this loop takes input value decide if it is in range 32 - 1 
            
                with open("lotto.txt","a") as f:
                        f.write(str(gs))
                        f.write(" ")
                still_guessing = False        
            
            else:
                
                if attempt < 10:
                    gs = input("Please enter number in range from 1 to 32: ")
                attempt = attempt + 1

range = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32']
   
gs1 = input("First numer: ")
check_guess(gs1,range)
gs2 = input("Second numer: ")
check_guess(gs2,range)
gs3 = input("Third numer: ")
check_guess(gs3,range)
gs4 = input("Fourth numer: ")
check_guess(gs4,range)

